# UI Challenge (Angular)
---
- Use the latest Angular to develop the UI (ChallengeUI.jpg);
- Open the ChallengeUI.xd file to take all the assets by exporting them;
![UI Challenge](https://bitbucket.org/usersgwebteam/ui-challenge-angular/raw/ffd47f5452990ad0e531a62b672746b5b6172ecf/ChallengeUI.jpg)
---
You are allow to use Material design, Bootstrap or any other library for styling.
---
### Material
```
npm install --save @angular/material @angular/cdk @angular/animations
```
---
### Bootstrap
```
npm install --save bootstrap jquery
```
Edit .angular-cli.json and put the bootstrap file inside
```
"styles": [
    "styles.css",
    "../node_modules/bootstrap/dist/css/bootstrap.min.css"
  ],
  "scripts": [
    "../node_modules/jquery/dist/jquery.min.js",
    "../node_modules/bootstrap/dist/js/bootstrap.min.js"
  ],
```
---
### After finish the challenge, please push your branch into the bucket and remember to .gitignore the node_modules folder.